# Petunjuk:

    $ cd
    $ mkdir skripsi
    $ cd skripsi
    $ pwd
    /home/nama-user/skripsi

    $ mkdir env
    $ cd env
    $ pwd
    /home/nama-user/skripsi/env

## mengaktifkan environment
    $ virtualenv skripsi
    $ source skripsi/bin/activate

## keluar dari folder env
    (skripsi)$ cd ..
    (skripsi)$ pwd
    /home/nama-user/skripsi

    (skripsi)$ git clone https://bitbucket.org/drayanaindra/skripsi_sigit.git

## kalau skripsi_sigit udah ada, lakukan ini

    (skripsi)$ cd skripsi_sigit/django_lms
    (skripsi)$ pwd
    /home/nama-user/skripsi/skripsi_sigit/django_lms

    (skripsi)$ pip install -r ../requirements.txt
    (skripsi)$ ./manage_local.py syncdb
    (skripsi)$ ./manage_local.py migrate
    (skripsi)$ ./manage_local.py createsuperuser
    (skripsi)$ ./manage_local.py runserver

## menjalankan sehabis shutdown

    $ source skripsi/env/skripsi/bin/activate
    $ cd skripsi/skripsi_sigit/django_lms
    $ ./manage_local.py runserver

