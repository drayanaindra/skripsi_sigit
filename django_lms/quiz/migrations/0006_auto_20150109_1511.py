# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0005_question_score'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='score',
            field=models.CharField(max_length=3, null=True, verbose_name=b'Score', blank=True),
        ),
    ]
