# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0003_auto_20150107_1758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='progress',
            name='is_score',
            field=models.DecimalField(verbose_name='Total Score', max_digits=5, decimal_places=2),
        ),
    ]
