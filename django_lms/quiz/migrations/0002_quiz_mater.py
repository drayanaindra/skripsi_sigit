# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0009_auto_20141216_0403'),
        ('quiz', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='quiz',
            name='mater',
            field=models.ForeignKey(verbose_name=b'Materi', blank=True, to='lms.Materi', null=True),
            preserve_default=True,
        ),
    ]
