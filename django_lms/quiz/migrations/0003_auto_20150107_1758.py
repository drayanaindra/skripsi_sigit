# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0002_quiz_mater'),
    ]

    operations = [
        migrations.AddField(
            model_name='progress',
            name='category',
            field=models.ForeignKey(default=1, verbose_name='Category', to='quiz.Category'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='progress',
            name='is_score',
            field=models.FloatField(default=0.0, verbose_name='Total Score'),
            preserve_default=False,
        ),
    ]
