# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0009_auto_20150109_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='progress',
            name='is_score',
            field=models.DecimalField(null=True, verbose_name='Total Score', max_digits=5, decimal_places=2, blank=True),
        ),
    ]
