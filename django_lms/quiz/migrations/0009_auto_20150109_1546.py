# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0008_auto_20150109_1515'),
    ]

    operations = [
        migrations.AlterField(
            model_name='progress',
            name='category',
            field=models.ForeignKey(verbose_name='Category', blank=True, to='quiz.Category', null=True),
        ),
    ]
