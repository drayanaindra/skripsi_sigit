from django.conf.urls import patterns, include, url

from views import create_account, login

urlpatterns = patterns('',
                       url(r'^create-account$', create_account, name='create-account'),
                       url(r'^signin$', login, name='account-siginin'),
                       )
