from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.template.response import TemplateResponse
from django.shortcuts import redirect

from utils import validate
from models import NIMLibrary, Profile


def create_account(request):
    check_validate = {}

    if request.POST:
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')

        check_validate = validate(username, email, password)
        if check_validate['message'] == 'error':
            return TemplateResponse(request, 'accounts/form_register.html', check_validate)

        nim = NIMLibrary.objects.get(nim=username)
        user = User.objects.create_user(nim.nim, email, password)
        user.first_name = nim.name
        user.save()
        siswa = User.objects.get(pk=user.id)
        profile = Profile.objects.create(user=siswa)
        profile.user_id = siswa.pk
        profile.save()

        return TemplateResponse(request, 'accounts/form_register.html', {'success': 'Tautan konfirmasi sudah di kirim ke email'})

    return TemplateResponse(request, 'accounts/form_register.html', check_validate)


def login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                return redirect('/')

    return TemplateResponse(request, 'accounts/form_login.html')
