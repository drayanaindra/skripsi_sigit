# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20141120_1610'),
    ]

    operations = [
        migrations.CreateModel(
            name='NIMLibrary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=70)),
                ('nim', models.CharField(max_length=20)),
                ('status', models.CharField(max_length=2, choices=[(b'SW', b'Siswa'), (b'GR', b'Guru'), (b'ST', b'Staf')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
