# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_nimlibrary'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nimlibrary',
            name='status',
            field=models.CharField(max_length=2, choices=[(b'GR', b'Guru'), (b'SW', b'Siswa')]),
        ),
    ]
