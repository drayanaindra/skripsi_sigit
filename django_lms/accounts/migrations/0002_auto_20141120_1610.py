# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0002_auto_20141120_1610'),
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='alamat',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='jabatan',
            field=models.CharField(default=b'GR', max_length=2, choices=[(b'GR', b'Guru'), (b'SW', b'Siswa')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='kelas',
            field=models.ForeignKey(blank=True, to='lms.Kelas', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='tanggal_lahir',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='telpon',
            field=models.CharField(max_length=12, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(related_name=b'profile_pengguna', verbose_name='Pengguna', to=settings.AUTH_USER_MODEL),
        ),
    ]
