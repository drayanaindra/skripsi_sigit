from django.contrib.auth.models import User
from models import NIMLibrary


def validate(username, email, password):
    validate = {'message': 'success'}
    status_mail = None

    try:
        NIMLibrary.objects.get(nim=username)
        status_usernama = True
    except NIMLibrary.DoesNotExist:
        status_usernama = False

    try:
        User.objects.get(username=username)
        username_is_register = True
    except User.DoesNotExist:
        username_is_register = False

    try:
        User.objects.get(email=email)
    except User.DoesNotExist:
        status_mail = False

    if not username or status_usernama is False:
        validate.update({'username': 'fail'})
        validate.update({'message': 'error'})
    elif username and username_is_register is True:
        validate.update({'username': username})
        validate.update({'username_status': username_is_register})
        validate.update({'message': 'error'})
    else:
        validate.update({'username': username})

    if not email:
        validate.update({'email': 'fail'})
        validate.update({'message': 'error'})
    elif email or status_mail is True:
        validate.update({'email': 'fail'})
        validate.update({'email_status': status_mail})
    else:
        validate.update({'email': email})
    if not password or len(password) <= 6:
        validate.update({'password': 'fail'})
        validate.update({'message': 'error'})

    return validate
