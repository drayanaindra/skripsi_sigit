from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from userena.models import UserenaBaseProfile

from lms.models import Kelas

JABATAN = (
        ('GR', 'Guru'),
        ('SW', 'Siswa'),
    )

class Profile(UserenaBaseProfile):
    user = models.OneToOneField(User,
                                unique=True,
                                verbose_name=_('Pengguna'),
                                related_name='profile_pengguna')
    alamat = models.TextField(null=True)
    kelas = models.ForeignKey(Kelas, null=True, blank=True)
    jabatan = models.CharField(choices=JABATAN, max_length=2, default='GR')
    telpon = models.CharField(max_length=12, null=True)
    tanggal_lahir = models.DateField(null=True)


class NIMLibrary(models.Model):
    name = models.CharField(max_length=70)
    nim = models.CharField(max_length=20)
    status = models.CharField(choices=JABATAN, max_length=2)

    def __unicode__(self):
        return self.name