# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('essay', '0002_essay_question_score'),
    ]

    operations = [
        migrations.AlterField(
            model_name='essay_question',
            name='score',
            field=models.IntegerField(null=True, verbose_name=b'Score', blank=True),
        ),
    ]
