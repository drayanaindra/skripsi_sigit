# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('essay', '0003_auto_20150109_1506'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='essay_question',
            name='score',
        ),
    ]
