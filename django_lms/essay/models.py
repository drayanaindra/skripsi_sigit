from django.utils.translation import ugettext as _
from quiz.models import Question
from django.db import models


from numpy import zeros
from scipy.linalg import svd
# following needed for TFIDF
from math import log
from numpy import asarray, sum


stopwords = ['dan', 'yang', 'untuk', 'di', 'dari', 'sebuah', 'ke']
ignorechars = ''',:'!'''

class LSA(object):

    def __init__(self, stopwords, ignorechars):
        self.stopwords = stopwords
        self.ignorechars = ignorechars
        self.wdict = {}
        self.dcount = 0

    def parse(self, doc):
        words = doc.split()
        for w in words:
            w = w.lower().translate(None, self.ignorechars)
            if w in self.stopwords:
                continue
            elif w in self.wdict:
                self.wdict[w].append(self.dcount)
            else:
                self.wdict[w] = [self.dcount]
        self.dcount += 1

    def build(self):
        self.keys = [k for k in self.wdict.keys() if len(self.wdict[k]) > 1]
        self.keys.sort()
        self.A = zeros([len(self.keys), self.dcount])
        for i, k in enumerate(self.keys):
            for d in self.wdict[k]:
                self.A[i, d] += 1

    def calc(self):
        """This is value
        """
        self.U, self.S, self.Vt = svd(self.A)

    def TFIDF(self):
        WordsPerDoc = sum(self.A, axis=0)
        DocsPerWord = sum(asarray(self.A > 0, 'i'), axis=1)
        rows, cols = self.A.shape
        for i in range(rows):
            for j in range(cols):
                self.A[i, j] = (
                    self.A[i, j] / WordsPerDoc[j]) * log(float(cols) / DocsPerWord[i])

    def printA(self):
        print 'Here is the count matrix'
        print self.A

    def printSVD(self):
        return -1 * self.Vt[0:3, :]


class Essay_Question(Question):


    def check_if_correct(self, guess):
        # if guess.lower() == self.explanation.lower():
        #     return True
        # else:
        #     return False
        return True

    def get_score(self, guess):
        # super(Essay_Question, self).get_score(guess)

        answere = ["{}".format(self.explanation.lower()), "{}".format(guess.lower())]
        
        lsa = LSA(stopwords, ignorechars)
        # import pdb; pdb.set_trace()
        for t in answere:
            lsa.parse(t)

        lsa.build()
        lsa.printA()
        lsa.calc()

        if self.explanation.lower() == guess.lower():
            score = self.score

        score = (int(self.score) * lsa.printSVD()[1][0])

        return score

    def get_point(self, ans):
        expl = self.explanation.lower()
        ans = asn.lower()

    def get_answers(self):
        return False

    def get_answers_list(self):
        return False

    def answer_choice_to_string(self, guess):
        return str(guess)

    def __unicode__(self):
        return unicode(self.content)

    class Meta:
        verbose_name = _("Essay style question")
        verbose_name_plural = _("Essay style questions")
