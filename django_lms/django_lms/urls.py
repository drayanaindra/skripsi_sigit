from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('userena.urls')),
    url(r'^q/', include('quiz.urls')),
    url(r'^u/', include('accounts.urls')),
    url(r'^', include('lms.urls')),
)
