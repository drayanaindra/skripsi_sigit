from base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': normpath(join('dev.db')),
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'