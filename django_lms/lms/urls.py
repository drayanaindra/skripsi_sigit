from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from views import InfoView, MateriView, follow_kelas, unfollow_kelas

urlpatterns = patterns('',
                       url(r'^information/$', InfoView.as_view(),
                           name='information'),
                       url(r'^materi/$',
                           login_required(MateriView.as_view()), name='materi'),
                       url(r'^follow-kelas/(?P<kelas_id>\d+)', follow_kelas, name='follow_kelas'),
                       url(r'^unfollow-kelas/$', unfollow_kelas, name='unfollow_kelas'),
                       url(r'^$', TemplateView.as_view(
                           template_name='lms/home.html'), name='lms-index'),
                       )
