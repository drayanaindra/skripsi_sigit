from datetime import datetime

from django.db import models
from django.contrib.auth.models import User


STATUS = (
    ('B', 'Baik'),
    ('C', 'Cukup'),
    ('K', 'Kurang'),
)

NOW = datetime.now()


class Information(models.Model):
    author = models.ForeignKey(User)
    title = models.CharField(max_length=250)
    info = models.TextField(verbose_name='Information')
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{a} - {t}".format(a=self.author, t=self.title)


class Ujian(models.Model):
    title = models.CharField(max_length=100)
    keterangan = models.TextField()
    tanggal_mulai = models.DateTimeField()
    tanggal_akhir = models.DateTimeField()
    tanggal = models.DateTimeField(auto_now_add=True, default=NOW)


class Kelas(models.Model):
    nama = models.CharField(max_length=25)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = 'Kelas'
        verbose_name_plural = 'Kelas'


class Pelajaran(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = 'Pelajaran'
        verbose_name_plural = 'Pelajaran'


class NilaiUjian(models.Model):
    siswa = models.ForeignKey(User)
    nilai = models.CommaSeparatedIntegerField(max_length=1024)
    status = models.CharField(max_length=1, choices=STATUS)
    tanggal = models.DateTimeField(auto_now_add=True, default=NOW)

    def __str__(self):
        return self.siswa


class SoalUjian(models.Model):
    ujian = models.ForeignKey(Ujian)
    soal = models.TextField()
    jawaban = models.TextField(null=True)
    tanggal = models.DateTimeField(auto_now_add=True, default=NOW)


class RiwayatJawaban(models.Model):
    soal = models.ForeignKey(SoalUjian)
    siswa = models.ForeignKey(User)
    jawaban = models.TextField(null=True)
    skor = models.CommaSeparatedIntegerField(max_length=1024)
    tanggal = models.DateTimeField(auto_now_add=True, default=NOW)


class SubKelas(models.Model):
    kelas = models.ForeignKey(Kelas)
    nama = models.CharField(max_length=50)

    def __str__(self):
        return "{k} - {n}".format(k=self.kelas, n=self.nama)

    class Meta:
        verbose_name = 'Sub Kelas'
        verbose_name_plural = 'Sub Kelas'

    @property
    def get_name_kelas(self):
        return "{k} - {n}".format(k=self.kelas, n=self.nama)


class Materi(models.Model):
    sub_kelas = models.ForeignKey(SubKelas, verbose_name='Sub Kelas')
    pelajaran = models.ForeignKey(Pelajaran, null=True)
    judul = models.CharField(max_length=100)
    desc = models.TextField(verbose_name='Deskripsi', help_text='Deskripsi kan materi pelajaran secara singkat')
    doc = models.FileField(upload_to='materi/', null=True)
    tanggal_unggah = models.DateTimeField(auto_now_add=True, default=NOW)

    def __str__(self):
        return self.judul

    class Meta:
        verbose_name = 'Materi'
        verbose_name_plural = 'Materi'


class FollowKelas(models.Model):
    sub_kelas = models.ForeignKey(SubKelas, verbose_name='Sub Kelas')
    user = models.ForeignKey(User, verbose_name='Siswa')

    def __str__(self):
        return "{s} - {u}".format(s=self.sub_kelas.nama, u=self.user)

    class Meta:
        verbose_name = 'Siswa Per Kelas'
        verbose_name_plural = 'Siswa Per Kelas'
