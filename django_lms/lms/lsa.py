from numpy import zeros
from scipy.linalg import svd
# following needed for TFIDF
from math import log
from numpy import asarray, sum

titles = ["Ekologi adalah ilmu yang mengkaji interaksi antar makhluk hidup dan lingkungan. Sumber lain menyebutkan ekologi adalah ilmu yang mempelajari hubungan timbal balik antara makhluk hidup dengan yang lain",
          "ilmu yang mempelajari interaksi antara makhluk hidup dan lingkungan.",
          # "hubungan timbal balik antara makhluk hidup dengan yang lain",
          # "ilmu yang mempelajari kaitan antara makhluk hidup dengan yang lain.",
          # "Ekologi adalah ilmu yang mengkaji interaksi antar makhluk hidup dan lingkungan. Sumber lain menyebutkan ekologi adalah ilmu yang mempelajari hubungan timbal balik antara makhluk hidup dengan yang lain",
          # "akan ada hal yang Suatu hari yang menyenangkan",
          # "akan Suatu hari yang menyenangkan dengan",
          # "dari mana suatu hari dimana akan menyenangkan",
          # "Investing in Real Estate, 5th Edition",
          # "Stock Investing For Dummies",
          # "Rich Dad's Advisors: The ABC's of Real Estate Investing: The Secrets of Finding Hidden Profits Most Investors Miss"
          ]
stopwords = ['dan', 'atau', 'untuk', 'di', 'dari', 'yang', 'ke']
ignorechars = ''',:'!'''


class LSA(object):

    def __init__(self, stopwords, ignorechars):
        self.stopwords = stopwords
        self.ignorechars = ignorechars
        self.wdict = {}
        self.dcount = 0

    def parse(self, doc):
        words = doc.split()
        for w in words:
            w = w.lower().translate(None, self.ignorechars)
            if w in self.stopwords:
                continue
            elif w in self.wdict:
                self.wdict[w].append(self.dcount)
            else:
                self.wdict[w] = [self.dcount]
        self.dcount += 1

    def build(self):
        self.keys = [k for k in self.wdict.keys() if len(self.wdict[k]) > 1]
        self.keys.sort()
        self.A = zeros([len(self.keys), self.dcount])
        for i, k in enumerate(self.keys):
            for d in self.wdict[k]:
                self.A[i, d] += 1

    def calc(self):
        """This is value
        """
        self.U, self.S, self.Vt = svd(self.A)

    def TFIDF(self):
        WordsPerDoc = sum(self.A, axis=0)
        DocsPerWord = sum(asarray(self.A > 0, 'i'), axis=1)
        rows, cols = self.A.shape
        for i in range(rows):
            for j in range(cols):
                self.A[i, j] = (
                    self.A[i, j] / WordsPerDoc[j]) * log(float(cols) / DocsPerWord[i])

    def printA(self):
        print 'count matrix'
        return self.A

    def printSVD(self):
        print 'singular values'
        print self.S
        print 'H3 columns of the U matrix'
        print -1 * self.U[:, 0:3]
        print 'the first 3 rows of the Vt matrix'
        svd = -1 * self.Vt[0:3, :]
        return svd 

mylsa = LSA(stopwords, ignorechars)
for t in titles:
    mylsa.parse(t)
mylsa.build()
mylsa.printA()
mylsa.calc()
mylsa.printSVD()
