from django.contrib import admin

from .models import (Information, Ujian, Kelas,
                     Pelajaran, Materi, NilaiUjian, SoalUjian,
                     RiwayatJawaban, SubKelas, FollowKelas)


admin.site.register(Information)
admin.site.register(FollowKelas)
admin.site.register(SubKelas)
admin.site.register(Kelas)
admin.site.register(Materi)
admin.site.register(Pelajaran)
