from django.views.generic import TemplateView
from django.contrib.auth.models import User
from django.shortcuts import redirect

from models import Information, Materi, SubKelas, FollowKelas
from essay.models import Essay_Question


class InfoView(TemplateView):
    template_name = 'lms/info.html'

    def get_context_data(self, **kwargs):
        context = super(InfoView, self).get_context_data(**kwargs)
        context['info'] = Information.objects.all().order_by('-created_on')
        return context


class MateriView(TemplateView):
    template_name = 'lms/kelas.html'

    def get_context_data(self, **kwargs):
        context = super(MateriView, self).get_context_data(**kwargs)
        context['is_follow_kelas'] = False

        if self.request.user in [item.user for item in FollowKelas.objects.filter(user=self.request.user)]:
            get_kelas = FollowKelas.objects.get(user=self.request.user)
            context['is_follow_kelas'] = True

            context['materi_by_kelas'] = Materi.objects.filter(
                sub_kelas=get_kelas.sub_kelas)

        context['kelas'] = SubKelas.objects.all()

        return context


def follow_kelas(request, kelas_id):
    try:
        get_kelas = SubKelas.objects.get(pk=kelas_id)
        get_user = User.objects.get(pk=request.user.id)
    except:
        return redirect('/materi/')

    try:
        FollowKelas.objects.get(user__id=request.user.id)
        is_follow = True
    except:
        is_follow = False

    if is_follow:
        return redirect('/materi/')

    FollowKelas.objects.create(sub_kelas=get_kelas, user=get_user)

    return redirect('/materi/')


def unfollow_kelas(request):

    try:
        get_kelas = FollowKelas.objects.get(user__id=request.user.id)
    except:
        return redirect('/materi/')

    FollowKelas.objects.get(user__id=request.user.id).delete()

    return redirect('/materi/')    
