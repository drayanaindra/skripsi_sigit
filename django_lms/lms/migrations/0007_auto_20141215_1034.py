# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0006_auto_20141215_0813'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='followkelas',
            options={'verbose_name': 'Siswa Per Kelas', 'verbose_name_plural': 'Siswa Per Kelas'},
        ),
        migrations.AlterModelOptions(
            name='kelas',
            options={'verbose_name': 'Kelas', 'verbose_name_plural': 'Kelas'},
        ),
        migrations.AlterModelOptions(
            name='materi',
            options={'verbose_name': 'Materi', 'verbose_name_plural': 'Materi'},
        ),
        migrations.AlterModelOptions(
            name='pelajaran',
            options={'verbose_name': 'Pelajaran', 'verbose_name_plural': 'Pelajaran'},
        ),
        migrations.AlterModelOptions(
            name='subkelas',
            options={'verbose_name': 'Sub Kelas', 'verbose_name_plural': 'Sub Kelas'},
        ),
        migrations.AddField(
            model_name='materi',
            name='desc',
            field=models.TextField(default='hello', help_text=b'Deskripsi kan materi pelajaran secara singkat', verbose_name=b'Deskripsi'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='materi',
            name='tanggal_unggah',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 10, 34, 28, 90228)),
        ),
        migrations.AlterField(
            model_name='nilaiujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 10, 34, 28, 90228), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 10, 34, 28, 90228), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='soalujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 10, 34, 28, 90228), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='ujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 10, 34, 28, 90228), auto_now_add=True),
        ),
    ]
