# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0014_auto_20150109_1502'),
    ]

    operations = [
        migrations.AlterField(
            model_name='materi',
            name='tanggal_unggah',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 9, 15, 6, 26, 624296), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='nilaiujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 9, 15, 6, 26, 624296), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 9, 15, 6, 26, 624296), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='soalujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 9, 15, 6, 26, 624296), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='ujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 9, 15, 6, 26, 624296), auto_now_add=True),
        ),
    ]
