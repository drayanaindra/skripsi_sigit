# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lms', '0004_auto_20141213_1842'),
    ]

    operations = [
        migrations.CreateModel(
            name='FollowKelas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sub_kelas', models.ForeignKey(verbose_name=b'Sub Kelas', to='lms.SubKelas')),
                ('user', models.ForeignKey(verbose_name=b'Siswa', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='materi',
            name='tanggal_unggah',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 7, 37, 1, 349106)),
        ),
        migrations.AlterField(
            model_name='nilaiujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 7, 37, 1, 349106), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 7, 37, 1, 349106), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='soalujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 7, 37, 1, 349106), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='ujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 7, 37, 1, 349106), auto_now_add=True),
        ),
    ]
