# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0003_auto_20141204_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='information',
            name='created_on',
            field=models.DateTimeField(default=datetime.date(2014, 12, 13), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='materi',
            name='tanggal_unggah',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 13, 18, 42, 8, 671977)),
        ),
        migrations.AlterField(
            model_name='nilaiujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 13, 18, 42, 8, 671977), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 13, 18, 42, 8, 671977), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='soalujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 13, 18, 42, 8, 671977), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='ujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 13, 18, 42, 8, 671977), auto_now_add=True),
        ),
    ]
