# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0005_auto_20141215_0737'),
    ]

    operations = [
        migrations.AddField(
            model_name='materi',
            name='sub_kelas',
            field=models.ForeignKey(default=1, verbose_name=b'Sub Kelas', to='lms.SubKelas'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='materi',
            name='tanggal_unggah',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 8, 13, 25, 767094)),
        ),
        migrations.AlterField(
            model_name='nilaiujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 8, 13, 25, 767094), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 8, 13, 25, 767094), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='soalujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 8, 13, 25, 767094), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='ujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 15, 8, 13, 25, 767094), auto_now_add=True),
        ),
    ]
