# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0007_auto_20141215_1034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='materi',
            name='tanggal_unggah',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 16, 4, 0, 14, 730918)),
        ),
        migrations.AlterField(
            model_name='nilaiujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 16, 4, 0, 14, 730918), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 16, 4, 0, 14, 730918), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='soalujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 16, 4, 0, 14, 730918), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='ujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 16, 4, 0, 14, 730918), auto_now_add=True),
        ),
    ]
