# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('lms', '0002_auto_20141120_1610'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='materi',
            name='indikator',
        ),
        migrations.RemoveField(
            model_name='materi',
            name='keterangan',
        ),
        migrations.RemoveField(
            model_name='materi',
            name='kompetensi',
        ),
        migrations.RemoveField(
            model_name='materi',
            name='pokok',
        ),
        migrations.RemoveField(
            model_name='materi',
            name='tanggal',
        ),
        migrations.RemoveField(
            model_name='soalujian',
            name='jawan',
        ),
        migrations.AddField(
            model_name='materi',
            name='doc',
            field=models.FileField(null=True, upload_to=b'materi/'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='materi',
            name='pelajaran',
            field=models.ForeignKey(to='lms.Pelajaran', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='materi',
            name='tanggal_unggah',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 4, 15, 42, 26, 586450)),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='nilaiujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 4, 15, 42, 26, 586450), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='riwayatjawaban',
            name='jawaban',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='soalujian',
            name='jawaban',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='soalujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 4, 15, 42, 26, 586450), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='soalujian',
            name='ujian',
            field=models.ForeignKey(default=None, to='lms.Ujian'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ujian',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 4, 15, 42, 26, 586450), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ujian',
            name='title',
            field=models.CharField(default=None, max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='nilaiujian',
            name='nilai',
            field=models.CommaSeparatedIntegerField(max_length=1024),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='skor',
            field=models.CommaSeparatedIntegerField(max_length=1024),
        ),
        migrations.AlterField(
            model_name='riwayatjawaban',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 4, 15, 42, 26, 586450), auto_now_add=True),
        ),
    ]
